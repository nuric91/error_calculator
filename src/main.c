#include "error_calculator.h"
int main() {
  ErrorNumber_t a = {.value = 1.1, .error = 0.1};
  ErrorNumber_t b = {.value = 13, .error = 1};
  ErrorNumber_t c = {.value = 10.5, .error = 0.7};
  ErrorNumber_t b_over_c = EDIV(b, c);
  EPRINT(b_over_c);
  ErrorNumber_t inside_brackets = ESUB(a, b_over_c);
  EPRINT(inside_brackets);
  ErrorNumber_t result = EMUL(inside_brackets, inside_brackets);
  EPRINT(result);
}