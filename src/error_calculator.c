#include "error_calculator.h"
#include <math.h>
#include <stdio.h>
#include <string.h>

// LOCAL PROTOTYPES
double calculate_relative_error(ErrorNumber_t a, ErrorNumber_t b);

ErrorNumber_t error_number_addition(ErrorNumber_t a, ErrorNumber_t b) {
  ErrorNumber_t result = {.value = a.value + b.value,
                          .error = a.error + b.error};
  return result;
}
ErrorNumber_t error_number_subtraction(ErrorNumber_t a, ErrorNumber_t b) {
  ErrorNumber_t result = {.value = a.value - b.value,
                          .error = a.error + b.error};
  return result;
}
ErrorNumber_t error_number_multiplication(ErrorNumber_t a, ErrorNumber_t b) {
  ErrorNumber_t result;
  result.value = a.value * b.value;
  result.error = fabs(calculate_relative_error(a, b) * result.value);
  return result;
}
ErrorNumber_t error_number_division(ErrorNumber_t a, ErrorNumber_t b) {
  // sanity check
  ErrorNumber_t result = {.value = 0, .error = 0};
  if (b.value == 0) {
    printf("error calling division. Divsion by 0. 0 value returned.");
    return result;
  }
  result.value = a.value / b.value;
  result.error = fabs(calculate_relative_error(a, b) * result.value);
  return result;
}
void print_error_number(ErrorNumber_t a) {
  printf("%f | %f \n", a.value, a.error);
}

// LOCAL PROTOTYPE IMPLEMENTATION
double calculate_relative_error(ErrorNumber_t a, ErrorNumber_t b) {
  printf("%f, %f\n", a.error / a.value, b.error / b.value);
  return a.error / a.value + b.error / b.value;
}