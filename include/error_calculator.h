/* this are the incldues if you want to use the error calculator */

/* use this struct to descriped a number with error.
if you want to use a normal number without error, still use this struct with error 0*/
#ifndef ERROR_CALCULATOR_H
#define ERROR_CALCULATOR_H
#define EADD(a, b) error_number_addition(a,b)
#define ESUB(a, b) error_number_subtraction(a,b)
#define EMUL(a,b) error_number_multiplication(a,b)
#define EDIV(a,b) error_number_division(a,b)
#define EPRINT(a) print_error_number(a)

typedef struct ErrorNumber{
    double value;
    double error;
}ErrorNumber_t;

ErrorNumber_t error_number_addition(ErrorNumber_t a, ErrorNumber_t b);
ErrorNumber_t error_number_subtraction(ErrorNumber_t a, ErrorNumber_t b);
ErrorNumber_t error_number_multiplication(ErrorNumber_t a, ErrorNumber_t b);
ErrorNumber_t error_number_division(ErrorNumber_t a, ErrorNumber_t b);

void print_error_number(ErrorNumber_t a);
#endif